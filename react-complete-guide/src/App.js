import './App.css';
import React , { Component } from "react";

// Imports müssen immer mit einem Großbuchstaben beginnen.
// Alle Elemente fangen mit einem Kleinbuchstaben an.
import Person from './Person/Person';

class App extends Component {
  state = {
    persondata: [
      { name: 'Lukas', age: 28 },
      { name: 'Jessi', age: 28 },
      { name: 'Moe', age: 28 }
    ],
    otherState: 'some other value'
  };

  switchNameHandler = () => {
    // console.log("Was clicked!");
    // Das funktioniert nicht, weil man den "state" nicht direkt ändern darf --> this.state.persons[0].name = "Nicht Lukas";
    this.setState({
      persondata: [
        { name: 'NOT Lukas', age: 28 },
        { name: 'Jessi', age: 28 },
        { name: 'Moe', age: 80 }
      ]
    })
  }

  render() {
    return (
      <div className="App">
        <h1>Hi, I'm a React App</h1>
        <p>This is really working!</p>
        <button onClick={this.switchNameHandler}>Switch Name</button>
        <Person
          name={this.state.persondata[0].name}
          age={this.state.persondata[0].age}/>
        <Person
          name={this.state.persondata[1].name}
          age={this.state.persondata[1].age}
          click={this.switchNameHandler}> My Hobbies: Racing </Person>
        <Person
          name={this.state.persondata[2].name}
          age={this.state.persondata[2].age}/>
      </div>
    )
    //return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Does this work now?'));
  }
}

export default App;