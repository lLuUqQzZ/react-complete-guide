import React from 'react';

// Props sind "Argumente" die benutzt werden können
const person = (props) => {
    return(
        <div>
            <p onClick={props.click}> Hi! Im {props.name} and im {props.age} years old!</p>
            <p>{props.children}</p>
        </div>
    )
};

export default person;