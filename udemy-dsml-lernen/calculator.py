
# Calculator


#add
def add(n1,n2):
    return n1 + n2

#substract
def substract(n1,n2):
    return n1 - n2

#multiply
def multiply(n1,n2):
    return n1 * n2

#divide
def divide(n1,n2):
    return n1 / n2

#operations
operations = {
   "+": add,
   "-": substract,
    "*": multiply,
   "/": divide
}

num1 = int(input("Whats the first number?: "))
num2 = int(input("Whats the second number?: "))

for symbol in operations:
    print(symbol)
operations_symbol = input("Pick an operation from the line above!")

calculation = operations[operations_symbol]
answer = calculation(num1,num2)

print(f"{num1} {operations_symbol} {num2} = {answer}")