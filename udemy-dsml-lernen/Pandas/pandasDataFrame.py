import numpy as np
import pandas as pd

from numpy.random import randn

np.random.seed(101)

df = pd.DataFrame(randn(5,4),["A","B","C","D","E"],["W","X","Y","Z"])

print(df)
# Dataframe ist eine Reihe von Series
#
#          W         X         Y         Z
#A  2.706850  0.628133  0.907969  0.503826
#B  0.651118 -0.319318 -0.848077  0.605965
#C -2.018168  0.740122  0.528813 -0.589001
#D  0.188695 -0.758872 -0.933237  0.955057
#E  0.190794  1.978757  2.605967  0.683509

# Eine Column zeigen -> sieht aus wie ein Series
print("#######################")
print("Eine Column zeigen -> sieht aus wie ein Series")
print("-----")
print(df["W"])
print(df[["W","Z"]])

# Neue Columns aus alten erzeugen
print("#######################")
print("Neue Columns aus alten erzeugen")
df["new"] = df["W"] + df["Y"]
print(df["new"])

print(df)
print(df.drop(labels="new", axis=1))
