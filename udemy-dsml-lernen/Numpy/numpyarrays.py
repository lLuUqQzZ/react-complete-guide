import numpy as np

#Vecroten sind 1-dimensionale Arrays, Matrix ist mehr-dimensional
# normale Python Liste
my_list = [1,2,3]
print("## normale Python Liste ##")
print(my_list)

#

# eine Normale Python Liste kann man mit numpy in ein Array casten
print("## eine Normale Python Liste kann man mit numpy in ein Array casten ##")
print(np.array(my_list))

# man kann eine Liste auch in ein mehrdimensionales Array casten
print("## man kann eine Liste auch in ein mehrdimensionales Array casten ##")
my_mat = [[1,2,3],[4,5,6],[7,8,9]]
print("normale Liste:")
print(my_mat)
print("mehrdimensionales Array:")
print(np.array(my_mat))

# bestimmte Werte aus dem Array bekommen mit brackets und slice notation
print("## bestimmte Werte aus dem Array bekommen mit brackets und slice notation: ##")
arr = np.arange(0,11)
print(arr)
print("Das 7. Element ausgeben: " + str(arr[7]))
print("Oder einen Teil aus dem Array schneiden: " + str(arr[1:5]))

# Werte aus einem 2D array ziehen
print("## Werte aus einem 2D-array ziehen: ##")
arr_2d = np.array([[5,10,15],[20,25,30],[35,40,45]])
print(arr_2d)
print("------")
sliced_arr_2d = arr_2d[:2,1:]
print(sliced_arr_2d)

#  Numpy Operations die man benutzen kann 

