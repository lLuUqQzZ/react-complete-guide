import numpy as np
from numpy import random

# Create an array of 10 zeros
print("Create an array of 10 zeros")
arr_ten_zeroes = np.zeros(10)
print(arr_ten_zeroes)
print("#####################")

# Create an array of 10 fives
print("Create an array of 10 fives")
arr_ten_fives = arr_ten_zeroes + 5
print(arr_ten_fives)
print("#####################")

# Create an array of the integers from 10 to 50
print("Create an array of the integers from 10 to 50")
arr_10to50 = np.arange(10,51)
print(arr_10to50)
print("#####################")

# Create an array of all the even integers from 10 to 50
print("Create an array of all the even integers from 10 to 50")
arr_10to50_even = np.arange(10,51,2)
print(arr_10to50_even)
print("#####################")

# Create a 3x3 matrix with values ranging from 0 to 8
print("Create a 3x3 matrix with values ranging from 0 to 8")
arr_3x3_08 = np.arange(9).reshape((3,3))
print(arr_3x3_08)
print("#####################")

# Create a 3x3 identity matrix
print("Create a 3x3 identity matrix")
arr_identity = np.identity(3)
print(arr_identity)
print("#####################")

# Use NumPy to generate a random number between 0 and 1
print("Use NumPy to generate a random number between 0 and 1")
rand_num = random.rand(1)
print(rand_num)
print("#####################")

# Use NumPy to generate an array of 25 random numbers sampled from a standard normal distribution
print("Use NumPy to generate an array of 25 random numbers sampled from a standard normal distribution")
rand_arr = np.random.randn(25)
print(rand_arr)
print("#####################")

# Create the following matrix
print("Create the following matrix:")
arr_01Step = np.arange(0.01,1.01,0.01)
print(arr_01Step)
print("#####################")

# Create an array of 20 linearly spaced points between 0 and 1
print("Create an array of 20 linearly spaced points between 0 and 1")
arr_linspace = np.linspace(0,1,20)
print(arr_linspace)
print("#####################")

# Indexing and Selection """" NICHT GANZ SICHER """"
print("Indexing and Selection")
mat = np.arange(1,26).reshape(5,5)
print(mat)
print("-- Slice/Index --")
sl1 = mat[2:5,1:5]
print(sl1)
print("-- Slice/Index --")
sl2 = mat[3,4]
print(sl2)
print("-- Slice/Index --")
sl3 = mat[:3,1:2]
print(sl3)

# Get the sum of all the values in mat
print("Get the sum of all the values in mat")
matSum = mat.reshape(25)
print(sum(matSum))

# Get the sum of all the columns in mat
print("Get the sum of all the columns in mat")
matSumCol = sum(mat)
print(matSumCol)